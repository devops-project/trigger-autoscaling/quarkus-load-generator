package org.yaich;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;


/**
 * 
 * Author : walid.yaich@gmail.com
 * Tested on Linux mint 20 / JDK 11
 * 
 * This class is designed to generate LOAD (RAM / CPU) so you can easily test your autoscaling configuration in your HA environment.
 * Please see README.md
 * 
 * To monitor CPU and RAM of the JVM, you can run ./jconsole that you can find in your JAVA_HOME
 * 
 * References :
 * https://quarkus.io/guides/getting-started 
 * https://alvinalexander.com/blog/post/java/java-program-consume-all-memory-ram-on-computer/
 * https://gist.github.com/SriramKeerthi/0f1513a62b3b09fecaeb
 * 
 */

@Path("/load-generator")
public class LoadGeneratorResource {


    private static final Logger LOGGER = Logger.getLogger(LoadGeneratorResource.class);

    /**
     * http://localhost:8080/load-generator/eat-ram/500 : will try to allocate 500 MB.
     * This method try to allocate the amount of memory passed in parameter
     * @param amount_of_ram_to_eat
     * @return String : "Out of memory" or "Memory successully allocated ...."
     */
    @GET
    @Path("/eat-ram/{amount_of_ram_to_eat}")
    @Produces(MediaType.TEXT_PLAIN)
    public String eatingRAM(@PathParam("amount_of_ram_to_eat") int amount){
        Runtime rt = Runtime.getRuntime();
        try {
            ArrayList<byte[]> arrayOfBytes = new ArrayList<>();
            int desiredMemoryAllocation = amount; 
            //Will try to eat "amount MB" of RAM, of course, GC will always try to release memory ...
            //You can stop GC : https://www.baeldung.com/jvm-epsilon-gc-garbage-collector
            int index = 0;

            while (index < desiredMemoryAllocation)
            {
              byte[] oneMegaArray = new byte[1048576];
              arrayOfBytes.add(oneMegaArray);
              index++;
            }            
        
        } catch (OutOfMemoryError e) {
            LOGGER.error("No more Memory !", e);
            return "I m Quarkus, I'm sorry :(\n Out of memory !";
        }

        LOGGER.info("Free memory after operation : " + Runtime.getRuntime().maxMemory() / (1024.0 * 1024.0 * 1024.0));

        LocalDateTime dateTime = LocalDateTime.now();        
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");

        return String.format("JVM Date time : %s%n" +
                            "I m Quarkus%nMemory allocated successfully !%n" +
                            "Free memory after operation : %.2f MB%n" +
                            "Maximum memory for the JVM : %.2fGiB%n", 
                            dateTime.format(formatter),
                            rt.freeMemory() / (1024.0 * 1024.0),
                            Runtime.getRuntime().maxMemory() / (1024.0 * 1024.0 * 1024.0));
    }


    /**
     * Explanation of this call http://localhost:8080/load-generator/cpu/70/4/10 :
     * 1) I want this application to consume 70% of CPU LOAD
     * 2) My CPU can process 4 parallel threads
     * 3) This operation should take 10 seconds
     * 
     * @param load 
     * @param nbThreads
     * @param duration
     */
    @GET
    @Path("/cpu/{load_to_generate}/{number_of_threads}/{duration}")
    @Produces(MediaType.TEXT_PLAIN)
    public String loadCpu(@PathParam("load_to_generate") double load, //for exemple : 80 percent of CPU
                            @PathParam("number_of_threads") int nbThreads, //for exemple : 4 threads
                            @PathParam("duration") long duration){ //for exemple : 10 second

        for (int thread = 0; thread < nbThreads; thread++) {
            new BusyThread("Thread" + thread, load / 100, duration * 1000).start();
            LOGGER.info("Working ... Thread : " + thread);
        }

        return "I m Quarkus\n" +nbThreads + " Threads will be running for " + 
                duration + " seconds; they will try to maintain "+ load +"% CPU";
    }
}